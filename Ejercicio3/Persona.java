/* Para cada uno de los siguientes ejercicios se pide:
    - Crear una clase ejecutable (que contenga el método main()),
    - Crear al menos 2 objetos y mostrarlos por pantalla. Utilice la documentación del API de java
como ayuda. (Link en aula virtual a documentación) */

/* Ejercicio 3: Codifique un programa que defina dos valores constantes que representan la edad mínima y la edad máxima de una persona para poder votar. Se pide:
Crear una clase Persona.
Comprobar que dada una persona se indique si la edad de la misma lo habilita para votar.
Incluir el método toString para devolver una representación del objeto.
Mostrar por pantalla si el mensaje “Votante habilitado” o “Votante inhabilitado en cada caso. */

package Ejercicio3;

public class Persona {
    // Atributos
    private int edad;

    // Metodos
    public void vote(int min, int max){
        if(this.edad >= min && this.edad <= max){
            System.out.println("Votante habilitado");
        }else{
            System.out.println("Votante inhabilitado");
        }
    }

    // Main
    public static void main(String[] args){
        int edadMinima = 18;
        int edadMaxima = 99;

        Persona persona1 = new Persona();
        persona1.edad = 13;
        Persona persona2 = new Persona();
        persona2.edad = 26;

        persona1.vote(edadMinima, edadMaxima);
        persona2.vote(edadMinima, edadMaxima);
    }
}
