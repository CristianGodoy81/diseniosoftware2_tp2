/* Para cada uno de los siguientes ejercicios se pide:
    - Crear una clase ejecutable (que contenga el método main()),
    - Crear al menos 2 objetos y mostrarlos por pantalla. Utilice la documentación del API de java como ayuda. (Link en aula virtual a documentación) */

/* Ejercicio 1: Elaborar un programa que dado un año indique si el mismo es bisiesto. */

/* Los años bisiestos se determinan siguiendo estas reglas:
    - Un año es bisiesto si es divisible por 4, pero no es divisible por 100, a menos que también sea divisible por 400.
    - Si un año es divisible por 400, es bisiesto sin importar si también es divisible por 100. */

package Ejercicio1;

public class Anio {
    // Atributos
    private int number;

    // Metodos
    public void isLeapYear(){
        if(this.number % 4 == 0 && this.number % 100 != 0){
            System.out.println(this.number + " es un año bisiesto");
        }else if(this.number % 400 == 0){
            System.out.println(this.number + " es un año bisiesto");
        }else{
            System.out.println(this.number + " no es un año bisiesto");
        }    
    }
    
    // Main
    public static void main(String[] args){
        
        Anio anio1 = new Anio();
        anio1.number = 2024;

        Anio anio2 = new Anio();
        anio2.number = 2023;

        anio1.isLeapYear();
        anio2.isLeapYear();
    }
}
