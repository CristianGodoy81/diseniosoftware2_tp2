/* Para cada uno de los siguientes ejercicios se pide:
    - Crear una clase ejecutable (que contenga el método main()),
    - Crear al menos 2 objetos y mostrarlos por pantalla. Utilice la documentación del API de java como ayuda. (Link en aula virtual a documentación) */

/* Ejercicio 2: Codifique una clase que dado una cadena de caracteres en minúscula permita la siguiente funcionalidad:
    - Mostrar la longitud de la cadena.
    - Mostrarla en minúscula.
    - Mostrar si contiene la letra ‘a’ e indicar en la posición en la que aparece.
    - Mostrarla en forma inversa.
    - Mostrar cada caracter de la cadena separado con un guion.
    - Mostrar la cantidad de vocales contenidas en la cadena de caracteres. */

package Ejercicio2;

public class Cadena {
    // Atributos
    private String text;

    // Metodos
    public void textLength(){ // Muestra la longitud.
        System.out.println("La longitud del texto es de " + this.text.length());
    }
    public void textLowerCase(){ // Muestra en minuscula.
        System.out.println("Texto en minuscula: " + this.text.toLowerCase());
    }
    public void textSearch(char x){ // Muestra si tiene una letra.
        int aux = this.text.indexOf(x);
        if(aux == -1){
            System.out.println("El caracter no pertenece al texto");
        }else{
            System.out.println("La primer aparicion del caracter, esta en la posicion: " + aux);
        }
    }
    public void textReverse(){ // Invierte un texto.
        // En proceso...
    }
    public void textWhithScript(){ // Muestra el texto separado por guiones.
        // En proceso...
    }
    public void numberOfVowels(){ // Muestra el numero de vocales.
        // En proceso...
    }

    // Main
    public static void main(String[] args){
        Cadena cadena1 = new Cadena();
        cadena1.text = "Hola mundo";
        Cadena cadena2 = new Cadena();
        cadena2.text = "Adios mundo cruel";

        cadena1.textLength();
        cadena2.textLength();

        cadena1.textLowerCase();
        cadena2.textLowerCase();

        cadena1.textSearch('a');
        cadena2.textSearch('a');
    }
}
