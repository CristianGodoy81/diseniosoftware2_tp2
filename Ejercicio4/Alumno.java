/* Para cada uno de los siguientes ejercicios se pide:
    - Crear una clase ejecutable (que contenga el método main()),
    - Crear al menos 2 objetos y mostrarlos por pantalla. Utilice la documentación del API de java
como ayuda. (Link en aula virtual a documentación) */

/* Ejercicio 4: Escribir un programa en el cual se debe almacenar información referida a alumnos (apellido y nombre, documento y nota).
Sobre ellos se pide:
Generar solo 5 alumnos.
    - Crear los respectivos métodos accesores para cada atributo.
    - Incluir el método toString para devolver una representación del objeto.
    - Mostrar la información de los alumnos almacenados.
    - Calcular el promedio general de las notas.
    - Mostrar la menor y la mayor nota. */

package Ejercicio4;

public class Alumno {
    // Atributo
    private String lastName; // Apellido
    private String name; // Nombre
    private int idCard; // Documento
    private double score; // Nota

    // Metodo
    public void info(){ // Muestra informacion del alumno.
        System.out.println("\n- DNI "+this.idCard+" - "+this.lastName+", "+this.name+" - Nota: "+this.score);
    }

    // Main
    public static void main(String[] args){

        Alumno alumno1 = new Alumno();
        alumno1.lastName = "Gomez";
        alumno1.name = "Santiago";
        alumno1.idCard = 40562147;
        alumno1.score = 4.75;

        Alumno alumno2 = new Alumno();
        alumno2.lastName = "Rodriguez";
        alumno2.name = "Sofia";
        alumno2.idCard = 48652147;
        alumno2.score = 8;

        Alumno alumno3 = new Alumno();
        alumno3.lastName = "Fernandez";
        alumno3.name = "Mateo";
        alumno3.idCard = 40561457;
        alumno3.score = 10;

        Alumno alumno4 = new Alumno();
        alumno4.lastName = "Perez";
        alumno4.name = "Valentina";
        alumno4.idCard = 39762147;
        alumno4.score = 6.5;

        Alumno alumno5 = new Alumno();
        alumno5.lastName = "Lopez";
        alumno5.name = "Franco";
        alumno5.idCard = 38952147;
        alumno5.score = 2.25;

        Alumno alumnos[] = {alumno1, alumno2, alumno3, alumno4, alumno5};

        // Promedio, menor y mayor nota
        double accumulator = 0; // Acumulador
        double smaller = 10; // Menor
        double bigger = 0; // Mayor
        for(int i=0; i<alumnos.length; i++){
            accumulator = accumulator + alumnos[i].score;
            if(alumnos[i].score < smaller){
                smaller = alumnos[i].score;
            }
            if(alumnos[i].score > bigger){
                bigger = alumnos[i].score;
            }
        }
        double average = accumulator / alumnos.length;

        alumno1.info();
        alumno2.info();
        alumno3.info();
        alumno4.info();
        alumno5.info();
        System.out.println("\n- El promedio general de las notas es: "+average);
        System.out.println("\n- La menor de las notas es: "+smaller);
        System.out.println("\n- La mayor de las notas es: "+bigger);
    }
}
