/* Para cada uno de los siguientes ejercicios se pide:
    - Crear una clase ejecutable (que contenga el método main()),
    - Crear al menos 2 objetos y mostrarlos por pantalla. Utilice la documentación del API de java
como ayuda. (Link en aula virtual a documentación) */

/* Ejercicio 5: Crear una clase producto, con los siguientes datos:
    - código del producto,
    - nombre del mismo,
    - precio de costo,
    - porcentaje de ganancia que se aplica sobre el precio de costo,
    - iva del producto, para todos los casos de 21%
    - precio de venta.
Crear los respectivos métodos accesores para cada atributo.
Mostrar el estado del producto.
Permitir determinar el precio de venta, el cual se obtiene de aplicarle el iva al resultado del precio de costo + precio de costo * el porcentaje de ganancia. El método creado no debe ser parte del protocolo de mensajes del objeto
Instanciar 2 productos e Indicar cuál de los 2 productos tiene el mayor precio de venta.
Incluir el método toString para devolver una representación del objeto.
 */

package Ejercicio5;

public class Producto {
    // Atributos
    private int code; // Codigo
    private String name; // Nombre
    private double costPrice; // Precio de costo
    private double percentageOfProfit; // Porcentaje de ganancia
    private double iva; // iva
    private double salePrice; // Precio de venta

    // Metodos
    public void info(){
        System.out.println("\n- Codigo: "+code);
        System.out.println("- Nombre: "+name);
        System.out.println("- Precio de costo: "+costPrice);
        System.out.println("- Porcentaje de ganancia: "+percentageOfProfit);
        System.out.println("- Iva: "+iva);
        System.out.println("- Precio de venta: "+salePrice);
    }

    // Main
    public static void main(String[] args){
        Producto producto1 = new Producto();
        producto1.code = 15486;
        producto1.name = "Nombre ficticio 1";
        producto1.costPrice = 100000;
        producto1.percentageOfProfit = 100;

        Producto producto2 = new Producto();
        producto2.code = 14865;
        producto2.name = "Nombre ficticio 2";
        producto2.costPrice = 50000;
        producto2.percentageOfProfit = 250;

        producto1.info();
        producto2.info();

    }
}
